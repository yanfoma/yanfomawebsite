// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue';
import YanfomaLayout from '~/layouts/YanfomaLayout.vue';
import ContactWidget from '~/components/ContactWidget.vue';
import ContactItem from '~/components/ContactItem.vue';
import Product from '~/components/Product.vue';
import SocialWidget from '~/components/SocialWidget.vue';
import About from '~/components/About.vue';
import Services from '~/components/Services.vue';
import PricingTable from '~/components/PricingTable.vue';
import Banner from '~/components/Banner.vue';
import BeOnlineQA from '~/components/BeOnlineQA.vue';
import Portfolio from '~/components/Portfolio.vue';
import PortfolioItem from '~/components/PortfolioItem.vue';
import WebShowcase from '~/components/WebShowcase.vue';
import Loader from '~/components/Loader.vue';
import Done from '~/components/Done.vue';
// Hostzina
import '~/assets/css/animate.css';
import '~/assets/css/iconfont.css';
import '~/assets/css/magnific-popup.css';
import '~/assets/css/jquery-ui.structure.min.css';
import '~/assets/css/jquery-ui.theme.min.css';
import '~/assets/css/bootstrap.min.css';
import '~/assets/css/font-awesome.min.css';
import '~/assets/css/plugins.css';
import '~/assets/css/style.css';
import '~/assets/css/responsive.css';

export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);
  Vue.component('YanfomaLayout', YanfomaLayout);
  Vue.component('ContactWidget', ContactWidget);
  Vue.component('ContactItem', ContactItem);
  Vue.component('Product', Product);
  Vue.component('SocialWidget', SocialWidget);
  Vue.component('About', About);
  Vue.component('Services', Services);
  Vue.component('PricingTable', PricingTable);
  Vue.component('Banner', Banner);
  Vue.component('BeOnlineQA', BeOnlineQA);
  Vue.component('Portfolio', Portfolio);
  Vue.component('PortfolioItem', PortfolioItem);
  Vue.component('WebShowcase', WebShowcase);
  Vue.component('Loader', Loader);
  Vue.component('Done', Done);

  head.script.push({
    src: '/js/jquery-2.1.1.js',
    body: true
  });
  head.script.push({
    src: '/js/jquery-ui.min.js',
    body: true
  });
  // head.script.push({
  //   src: '/js/Popper.js',
  //   body: true
  // });
  head.script.push({
    src: '/js/bootstrap.min.js',
    body: true
  });
  head.script.push({
    src: '/js/smoothscroll.js',
    body: true
  });
  head.script.push({
    src: '/js/custom.js',
    body: true
  });
  head.script.push({
    src: '/js/date_init.js',
    body: true
  });
  // head.script.push({
  //   src: '/js/main.js',
  //   body: true
  // });
}
